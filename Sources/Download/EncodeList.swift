import Files
import Foundation

public struct LutsCategory: Codable {
    public var categories: [String: [String]]
}

struct ExportList {
    func exportList() -> LutsCategory {
        var list = LutsCategory(categories: [:])
        list.categories["Cold"] = ["Lisbon", "London"]
        list.categories["Monochrome"] = ["Lisbon", "London"]
        list.categories["Warm"] = ["Lisbon", "London"]
        list.categories["Film"] = ["Lisbon", "London"]
        list.categories["Natural"] = ["Lisbon", "London"]
        list.categories["Contrasting"] = ["Lisbon", "London"]
        return list
    }
    func matchWithSheet() {
        let list = GaugeListMaker().openListFromDisk()
        var categories = GaugeListMaker().openCategoryes()
        
        var nofind: [String] = []
        
        categories.categories.forEach { element in
            var element = element
            let values = element.value.map { filtername in
                let f = list.first { filename in
                    filename.uppercased().contains(filtername.uppercased())
                }
                if f == nil { nofind.append(filtername) }
                return f
            }.map { opt in
                return opt ?? "nofile"
            }
            element.value = values
            categories.categories.updateValue(element.value, forKey: element.key)
        }
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let json = try! encoder.encode(categories)
        let file = try! Folder(path: "~/Desktop/").createFile(named: "LutsCategoriesMatched.json")
        try! file.write(json)
        
        print("nofind: \(nofind)")
    }
}
