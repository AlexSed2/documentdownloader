import Foundation
public struct LutsCategorySwamp: Codable {
    
    public struct Filter: Codable {
        public var premium: Bool
        public let url: String
    }
    
    public struct Category: Codable {
        public var name: String
        public var filters: [Filter]
        public let translate: [String: String]
        public let color: String
    }
    
    public var categories: [Category]
}

public extension LutsCategorySwamp {
    mutating func buyAll() {
        categories = categories.map { category in
            var category = category
            category.filters = category.filters.map { filter in
                var filter = filter
                filter.premium = false
                return filter
            }
            return category
        }
    }
}

public struct DownloadConfig {
    public var request: URLRequest { URLRequest(url: URL(string: "https://ams-apps.net/ru/filters/luts.json")!) }
    public init() {}
    public func makeList(apimodel: LutsCategorySwamp) -> [String] {
        apimodel.categories.reduce(into: [String]()) { $0 += $1.filters.map {
            let url = URL(string: $0.url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            let name = url!.lastPathComponent
            return name
        } }
    }
}

@available(macOS 12.0.0, iOS 15.0.0,  *)
struct QuickTest {
    func sequence() async {
        do {
            let (data, _) = try await URLSession.shared.data(for: DownloadConfig().request)
            let model = try JSONDecoder().decode(LutsCategorySwamp.self, from: data)
            let list = DownloadConfig().makeList(apimodel: model)
            let downloader = DocumentDownloader(foldername: "Luts",
                                                requiredList: list.reversed(),
                                                serverApi: "https://ams-apps.net/ru/filters") {} onDone: {}
            for await item in downloader {
                print("downloaded: \(item.name)")
            }
        } catch {}
    }
    func downloadCongig() async {
        do {
            let (data, _) = try await URLSession.shared.data(for: DownloadConfig().request)
            let model = try JSONDecoder().decode(LutsCategorySwamp.self, from: data)
            let list = DownloadConfig().makeList(apimodel: model)
            print("list: \(list)")
        } catch let error {
            print("error: \(error)")
        }
    }
    func downloadCongigAndBuy() async {
        do {
            let (data, _) = try await URLSession.shared.data(for: DownloadConfig().request)
            var model = try JSONDecoder().decode(LutsCategorySwamp.self, from: data)
            model.buyAll()
            model.categories.forEach {
                print("-")
                $0.filters.forEach { print($0) }
            }
        } catch let error {
            print("error: \(error)")
        }
    }
    func downloadList() async {
        do {
            let (data, _) = try await URLSession.shared.data(for: DownloadConfig().request)
            let model = try JSONDecoder().decode(LutsCategorySwamp.self, from: data)
            let list = DownloadConfig().makeList(apimodel: model)
            
            DocumentDownloader(foldername: "Luts",
                               requiredList: list.reversed(),
                               serverApi: "https://ams-apps.net/ru/filters") {  print("file downloaded")} onDone: {}.run()
            
        } catch let error {
            print("error: \(error)")
        }
    }
    func printPure(json: Data) {
        let z = try! JSONSerialization.jsonObject(with: json, options: .fragmentsAllowed)
        print("z: \(z)")
    }
}
