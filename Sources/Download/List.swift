import Files
import Foundation


struct GaugeListMaker {
    
    func make() -> [String] {
        let folder = try! Folder(path: "~/Desktop/Luts/")
        return folder.files.map { $0.name }
    }
    
    func createAndSaveModelFile() {
        let list = make()
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let json = try! encoder.encode(list)
        let file = try! Folder(path: "~/Desktop/").createFile(named: "Lutlist.json")
        try! file.write(json)
    }
    
    func openListFromDisk() -> [String] {
        let file = try! Folder(path: "~/Desktop/").file(named: "Lutlist.json")
        let data = try! file.read()
        let list = try! JSONDecoder().decode([String].self, from: data)
        let out = list.map { name in
            String(name.split(separator: ".").first! + ".zip")
        }
        return out
    }
    
    func createTemplateJson() {
        let model = ExportList().exportList()
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let json = try! encoder.encode(model)
        let file = try! Folder(path: "~/Desktop/").createFile(named: "LutsCategories2.json")
        try! file.write(json)
    }
    
    func openCategoryes() -> LutsCategory {
        let file = try! Folder(path: "~/Desktop/").file(named: "LutsCategories.json")
        let data = try! file.read()
        let list = try! JSONDecoder().decode(LutsCategory.self, from: data)
        return list
    }
    
    func openCategoryesMatched() {
        let file = try! Folder(path: "~/Desktop/").file(named: "LutsCategoriesMatched.json")
        let data = try! file.read()
        let list = try! JSONDecoder().decode(LutsCategory.self, from: data)
        print(list)
    }
}


