import Files
import Foundation
import Zip

@available(macOS 12.0.0, iOS 15.0.0, *)
public struct Iterator: AsyncIteratorProtocol {
    
    var gauge: Gauge    
    let serverApi: String
    let zipfolder: Folder
    let unzipFolder: Folder
    
    public mutating func next() async -> (name: String, url: URL)? {
        guard gauge.compleet != true else {
            print("download compleet")
            return nil }
        guard let first = gauge.diff.first else { return nil }
        guard let string = "\(serverApi)/\(first)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return nil }
        guard let url = URL(string: string) else { return nil }
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            let file = try zipfolder.createFile(named: first)
            try file.write(data)
            try Zip.unzipFile(file.url, destination: unzipFolder.url, overwrite: false, password: nil, progress: nil, fileOutputHandler: nil)
            gauge.locale.append(first)
            return (first, url)
        } catch let error {
            print("nil: \(error)")
            return nil
        }
    }
}

@available(macOS 12.0.0, iOS 15.0.0, *)
extension DocumentDownloader: AsyncSequence {
    public typealias AsyncIterator = Iterator
    public typealias Element = (name: String, url: URL)
    public __consuming func makeAsyncIterator() -> Iterator {
        let folder = getZipFolder()!
        let located = folder.files.map { $0.name }
        let gauge = Gauge(required: requiredList, located: located)
        return Iterator(gauge: gauge, serverApi: serverApi, zipfolder: getZipFolder()!, unzipFolder: getUnzipFolder()!)
    }
}
