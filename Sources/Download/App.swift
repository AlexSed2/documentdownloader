public struct DownloadApp {
    public init() {}
    public func run() {
        print("run")
    }
    public func runUtils() {
        GaugeListMaker().openCategoryesMatched()
    }
    @available(macOS 12.0.0, iOS 15.0.0,  *)
    public func testLoading() async {
        await QuickTest().downloadCongigAndBuy()
    }
}
