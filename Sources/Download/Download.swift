import Foundation
import Files
import Collections
import Zip

/**
 Создаёт папку в Documents и скачивает в неё список файлов,
 для начала работы нужно инициализировать объект, и сказать ему run().
 Так же есть место для статичного инстанса shared.
 */
public class DocumentDownloader {
    
    public static var shared: DocumentDownloader? 
    
    let foldername: String
    let requiredList: [String]
    let serverApi: String
    let onLoadAFile: (() -> Void)?
    let onDone: (() -> Void)?
    var corrupted = [File]() 
    
    /// - Parameters:
    ///     - foldername: название создаваемой папки в Docements
    ///     - serverApi: формат https://ams-apps.net/ru/filters (без слеша вконце)
    ///     - requiredList: список файлов, которые надо скачать
    ///     - onLoadAFile:Вызывается после каждого скаченного файла
    public init(foldername: String,
                requiredList: [String],
                serverApi: String,
                onLoadAFile: (() -> Void)? = nil,
                onDone: (() -> Void)? = nil) {
        self.foldername = foldername
        self.requiredList = requiredList
        self.serverApi = serverApi
        self.onLoadAFile = onLoadAFile
        self.onDone = onDone
    }
    
    public var downloadFolder: Folder? {
        getUnzipFolder()
    }
    
    /// Поочередно запускает таски от URLSession и сохранят скачанные файлы в папку
    /// Загрузка происходит в том порядке, в котором находятся имена файлов в массиве,
    /// посылаемом при инициализации.
    public func run() {
        guard let folder = getZipFolder() else { print("no folder"); return }
        let located = folder.files.map { $0.name }
        let gauge = Gauge(required: requiredList, located: located)
        recursiveDownload(gauge: gauge, folder: folder)
    }
    
    func recursiveDownload(gauge: Gauge, folder: Folder) {
        var gauge = gauge
        guard gauge.compleet != true else {
            print("Download complete")
            for file in corrupted {
                print("deleting: \(file)")
                try? file.delete()
            }
            onDone?()
            return
        }
        guard let first = gauge.diff.first else { return }
        download(name: first) { (name, data) in
            var error = ""
            var f: File?
            do {
                let file = try folder.createFile(named: name)
                f = file
                try file.write(data)
                try self.unzip(path: file.url, name: name)
            } catch let err {
                error = "\(err)"
                if err is ZipError {
                    if let file = f {
                        self.corrupted.append(file)
                    }
                }
            }
            gauge.locale.append(name)
            self.onLoadAFile?()
            print("file \(name) was downloaded, \(gauge.diff.count) remains, \(error)")
            self.recursiveDownload(gauge: gauge, folder: folder)
        } error: {
            DispatchQueue.global().asyncAfter(wallDeadline: .now() + .milliseconds(5000)) {
                self.recursiveDownload(gauge: gauge, folder: folder)                
            }
        }
    }
    
    func unzip(path: URL, name: String) throws {
        let url = getUnzipFolder()!.url
        try Zip.unzipFile(path, destination: url, overwrite: false, password: nil, progress: nil, fileOutputHandler: nil)
    }
    
    func download(name: String, handle: @escaping (String, Data) -> Void, error: @escaping  () -> Void) {
        guard let string = "\(serverApi)/\(name)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            print("no string")
            return
        }
        guard let url = URL(string: string) else { 
            print("no url")
            return 
        }
        download(url: url) { data in
            handle(name, data)
        } errorOccure: {
            error()
        }
    }
    
    func download(url: URL, handle: @escaping (Data) -> Void, errorOccure: @escaping  () -> Void) {
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard error == nil else { 
                print("error: \(error!)")
                errorOccure()
                return 
            }
            guard let data = data else { 
                print("no data")
                errorOccure()
                return 
            }
            handle(data)
        }
        task.resume()
    }
    
    // Utils
    
    private func getLocaleFolder() -> Folder? {
        let name = foldername
        if let folder = try? Folder.documents?.subfolder(named: name) {
            return folder
        } else {
            let folder = try? Folder.documents?.createSubfolder(named: name)
            return folder
        }
    }
    
    func getZipFolder() -> Folder? {
        let name = "Zip"
        if let folder = try? getLocaleFolder()?.subfolder(named: name) {
            return folder
        } else {
            let folder = try? getLocaleFolder()?.createSubfolder(named: name)
            return folder
        }
    }
    
    func getUnzipFolder() -> Folder? {
        let name = "Unzip"
        if let folder = try? getLocaleFolder()?.subfolder(named: name) {
            return folder
        } else {
            let folder = try? getLocaleFolder()?.createSubfolder(named: name)
            return folder
        }
    }
    
}


struct Gauge {
    
    var locale: OrderedSet<String>
    let gauge: OrderedSet<String>
    
    var diff: OrderedSet<String> { gauge.subtracting(locale) } // as gaugue >= locale
    var compleet: Bool { diff.count == 0 }
    
    init(required rlist: [String], located llist: [String]) {
        gauge = OrderedSet(rlist)
        locale = OrderedSet(llist)
    }
    
}
