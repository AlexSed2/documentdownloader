// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "plasticaDownload",
    products: [
        .library(
            name: "plasticaDownload",
            targets: ["Download"]),
    ],
    dependencies: [
        .package(url: "https://github.com/JohnSundell/Files", .branch("master")),
        .package(url: "https://github.com/apple/swift-collections", .branch("main")),
        .package(url: "https://github.com/marmelroy/Zip", .branch("master"))
    ],
    targets: [
        .target(
            name: "Download",
            dependencies: ["Files",
                           .product(name: "Collections", package: "swift-collections"),
                           .product(name: "Zip", package: "Zip")]),
        .executableTarget(
            name: "Runner",
            dependencies: ["Download"]),
    ]
)
