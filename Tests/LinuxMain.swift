import XCTest

import plasticaDownloadTests

var tests = [XCTestCaseEntry]()
tests += plasticaDownloadTests.allTests()
XCTMain(tests)
